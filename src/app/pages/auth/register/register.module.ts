import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';
import { TopoLoginModule } from 'src/app/pages/auth/topo-login/topo-login.module';

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPageRoutingModule,
    TopoLoginModule
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {}
