import { RegisterModel } from './models/register.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../login/service/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  user: RegisterModel;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService
    ) { }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
        nome: ['',[Validators.required],Validators.minLength(2), Validators.maxLength(19)],
        snome: ['',[Validators.required],Validators.minLength(2), Validators.maxLength(19)],
        email: ['', [Validators.email, Validators.required]],
        password: ['', [Validators.required, Validators.minLength(8)]],
        confirmPass:['',[Validators.required, Validators.minLength(8)]]
      });
  }
  createUser(){
    this.user = this.registerForm.value;
    this.loginService.createUser(this.user);
  }

}
