export interface RegisterModel {
    nome: string;
    snome: string;
    email: string;
    password: string;
    confirmPass: string;
}
