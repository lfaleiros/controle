import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../login/model/user.model';
import { LoginService } from '../login/service/login.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {

  forgotForm: FormGroup;
  usuario: UserModel;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService
    ) { }


  ngOnInit() {
      this.forgotForm = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]]
      });
    }

    recoverPass(){
      this.loginService.recoverPass(this.forgotForm.value.email);
    }
}
