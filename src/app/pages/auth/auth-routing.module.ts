import { TopoLoginModule } from './topo-login/topo-login.module';
import { TopoLoginComponent } from './topo-login/topo-login.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ForgotPage } from './forgot/forgot.page';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '', component: LoginPage
      },
      {
        path: 'forgot',
        component: ForgotPage
      },
      {
        path: 'register',
        component: RegisterPage
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    TopoLoginModule,
    RouterModule.forChild(routes)],
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage
  ]
})
export class AuthRoutingModule { }
