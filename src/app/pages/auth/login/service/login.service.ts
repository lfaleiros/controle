import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { RegisterModel } from '../../register/models/register.model';
import { UserModel } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedIn: Observable<any>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toastCtrl: ToastController,
  ) {
    this.isLoggedIn = auth.authState;
  }

  login(user: UserModel){
    this.auth.signInWithEmailAndPassword(user.email, user.password)
    .then(() =>
      this.toastMessage('Login com sucesso', 'success')
        .then(toast => toast.present)
          .then(() => this.routerFoward('home')))
    .catch(() =>
      this.toastMessage('Dados de acesso incorretos', 'danger')
      .then(x => x.present()));
  }

  private toastMessage(message: string, color: string): Promise<HTMLIonToastElement> {
    return this.toastCtrl.create({
       message,
       duration: 1500,
       color
     });
   }

   createUser(user: RegisterModel) {
    this.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then(credentials => console.log(credentials));
  }


  recoverPass(email: string) {
    this.auth.sendPasswordResetEmail(email)
    .then(() => this.routerBack('auth'))
    .catch(() => this.toastMessage('Email incorreto', 'danger').then(x => x.present()));
  }

  public routerFoward(rota: string){
    this.nav.navigateForward(rota);
  }

  public routerBack(rota: string){
    this.nav.navigateBack(rota);
  }

  public logout(){
    this.auth.signOut().then(() => this.routerBack('auth'));
  }
}
