import { UserModel } from './model/user.model';
import { LoginService } from './service/login.service';
import { FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  loginForm: FormGroup;
  usuario: UserModel;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService
    ) { }

  ngOnInit() {
    this.isUserLoggedIn();
    this.loginForm = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]],
        password: ['', [Validators.required, Validators.minLength(8)]]
      });
  }

  isUserLoggedIn() {
    this.loginService.isLoggedIn.subscribe(
      user => {
        if (user){

          this.loginService.routerFoward('home');
        }
      }
    );
  }
  login(){
      this.usuario = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      };
      this.loginService.login(this.usuario);
  }

}
