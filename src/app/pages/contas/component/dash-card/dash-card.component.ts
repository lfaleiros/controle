import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash-card',
  templateUrl: './dash-card.component.html',
  styleUrls: ['./dash-card.component.scss'],
})
export class DashCardComponent implements OnInit {
  @Input() color;
  @Input() title;
  @Input() badgeValor;
  @Input() text;
  @Input() icon;
  @Input() tipo;

  constructor() { }

  ngOnInit() {
    switch (this.tipo) {
      case 'pagar':
        this.pagar();
        break;
      case 'receber':
        this.receber();
        break;
      case 'saldo':
        this.saldo();
        break;

        break;

      default:
        break;
    }
  }
   pagar() {


    this.color = this.color ? this.color : 'danger';
    this.title = this.title ? this.title : 'Contas a pagar';
    this.icon = this.icon ? this.icon : 'arrow-down';
  }


  receber() {

    this.color = this.color ? this.color : 'primary';
    this.title = this.title ? this.title : 'Contas a Receber';
    this.icon = this.icon ? this.icon : 'arrow-up';
  }
  saldo() {

    this.color = this.color ? this.color : 'medium';
    this.title = this.title ? this.title : 'Saldo';
    this.icon = this.icon ? this.icon : 'wallet';

  }

}
