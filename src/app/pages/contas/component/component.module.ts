import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { DashCardComponent } from './dash-card/dash-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [DashCardComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  exports: [
    DashCardComponent
  ]
})
export class ComponentModule { }
