
export interface ContasModel{
    id?: string;
    valor: number;
    parceiro: string;
    descricao: string;
    tipo: string;
    dia: number;
    mes: number;
    ano: number;
}
