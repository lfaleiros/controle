import { DateHelper } from './../../helpers/date.helpers';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { ContasModel } from '../models/contas.model';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ContasService {

  collection: AngularFirestoreCollection;

  registraConta(contas: ContasModel) {
    contas.id = this.dataBase.createId();
    this.collection = this.dataBase.collection('conta');
    return this.collection.doc(contas.id).set(contas);
  }

  lista(tipo: string) {
    this.collection = this.dataBase.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remover(conta: ContasModel) {
   this.collection = this.dataBase.collection('conta');
   this.collection.doc(conta.id).delete();
  }

  editar(conta: ContasModel) {
    this.collection = this.dataBase.collection('conta');
    this.collection.doc(conta.id).update(conta);
  }

  /**
   * Totaliza as contas de acordo com o tipo
   * @Param tipo: string
   */
  pegarContas(tipo: string, date: string){
      const aux = DateHelper.quebraData(date);
      this.collection = this.dataBase.collection('conta',
        ref =>
        ref.where('tipo', '==', tipo)
          .where('mes','==',aux.mes)
          .where('ano', '==',aux.ano));
      return this.collection.get().pipe(map(snapShot => {
      let sum = 0;
      let cont = 0;
      snapShot.docs.map(doc => {
        const conta = doc.data();
        const valor = parseFloat(conta.valor);
        sum += valor;
        cont ++;
      });
      return {quantidade: cont, valor: sum};
    }));
  }

  constructor(
    private dataBase: AngularFirestore
  ) { }
}
