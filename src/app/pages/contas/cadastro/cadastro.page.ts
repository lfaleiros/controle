import { DateHelper } from './../../helpers/date.helpers';
import { NavController } from '@ionic/angular';
import { ContasService } from './../services/contas.service';
import { ContasModel } from './../models/contas.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  contasForm: FormGroup;
  contas: ContasModel;

  constructor( private formBuilder: FormBuilder,
               private contasService: ContasService,
               private nav: NavController) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.contasForm = this.formBuilder.group({
      valor: ['', [Validators.required, Validators.min(0.01)]],
      parceiro: ['', [Validators.required, Validators.minLength(5)]],
      descricao: ['', [Validators.required, Validators.minLength(6)]],
      tipo: ['', [Validators.required, Validators.minLength(2)]],
      date: [new Date().toISOString(), [Validators.required]],
    });
  }
  /**
   * Registra a nova conta no Firebase
   */
  registra(){
    this.colocarValornoContas();
    this.contasService.registraConta(this.contas).then(() => this.nav.navigateForward('home'));
  }

  colocarValornoContas(){
    const date = this.contasForm.get('date').value;
    this.contas = { ...this.contasForm.value, ...DateHelper.quebraData(date) };
  }

}
