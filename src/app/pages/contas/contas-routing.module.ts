import { ListaContasPage } from './lista-contas/lista-contas.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', children: [
      {
        path: 'pagar', component: ListaContasPage
      },
      {
        path: 'receber', component: ListaContasPage
      },
      {

        path: 'cadastro', component: CadastroPage
      },
      {
        path: 'relatorio', component: RelatorioPage
      }
    ]
  },
  {
    path: 'lista-contas',
    loadChildren: () => import('./lista-contas/lista-contas.module').then( m => m.ListaContasPageModule)
  }

];

@NgModule({
  imports: [CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations:[
    ListaContasPage,
    CadastroPage,
    RelatorioPage
  ]
})
export class ContasRoutingModule { }
