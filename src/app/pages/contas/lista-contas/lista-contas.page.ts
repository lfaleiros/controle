import { ContasService } from './../services/contas.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ContasModel } from '../models/contas.model';

@Component({
  selector: 'app-lista-contas',
  templateUrl: './lista-contas.page.html',
  styleUrls: ['./lista-contas.page.scss'],
})
export class ListaContasPage implements OnInit {
  url: string;
  contas;
  constructor(private router: Router,
              private contaService: ContasService,
              private alert: AlertController) { }

  ngOnInit() {
     this.url = this.router.url.split('/')[2];
     this.contaService.lista(this.url).subscribe(x => this.contas = x);
     this.url = this.url.charAt(0).toUpperCase() + this.url.substr(1);
  }

  async remover(conta){
    const confirm = await this.alert.create({
      header: 'Remover Conta',
      message: 'Deseja realmente apagar essa conta ?',
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Deletar',
        handler: () => this.contaService.remover(conta)
      }]
      });
    confirm.present();

  }

  async editar(conta: ContasModel){
    const confirm = await this.alert.create({
      header: 'Editar Conta',
      message: 'Deseja realmente editar essa conta ?',
      inputs: [
        {
          placeholder: 'Parceiro',
          name: 'parceiro',
          type: 'text',
          value: conta.parceiro
        },
        {
          placeholder: 'Descrição',
          name: 'descricao',
          type: 'text',
          value: conta.descricao
          },
        {
          placeholder: 'Valor',
          name: 'valor',
          type: 'number',
          value: conta.valor
          },
      ],
      buttons: [{
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Editar',
        handler: (data) => {
          const mergedData = { ...conta, ...data } as ContasModel;
          this.contaService.editar(mergedData);}
      }]
    });
    confirm.present();
  }
}
