import { ContasService } from './../contas/services/contas.service';
import { Component } from '@angular/core';
import { LoginService } from '../auth/login/service/login.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  date = new Date().toISOString();
   conta = {
     pagar:{quantidade: 0, valor: 0},
     receber:{quantidade: 0, valor: 0},
     saldo:{quantidade: 0, valor: 0}
   };
  constructor(
    private loginService: LoginService,
    private contaService: ContasService
  ) {}

  ionViewWillEnter(){
    this.atualizarConta();
  }

  atualizarConta(){

    this.contaService.pegarContas('pagar', this.date).subscribe(contas => {

      this.conta.pagar = contas;

      this.contaService.pegarContas('receber', this.date).subscribe(conta => {

        this.conta.receber = conta;
        this.calcularSaldo();
      });
    });
  }
  calcularSaldo(){
    this.conta.saldo.quantidade = this.conta.pagar.quantidade + this.conta.receber.quantidade;
    this.conta.saldo.valor = this.conta.receber.valor - this.conta.pagar.valor ;
  }

  logout(){
    this.loginService.logout();
  }
}
