// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebseConfig: {
    apiKey: 'AIzaSyDFUy9JoEbnEFtetS8oUHa77ymnKaXOvwM',
    authDomain: 'controle-if2.firebaseapp.com',
    databaseURL: 'https://controle-if2.firebaseio.com',
    projectId: 'controle-if2',
    storageBucket: 'controle-if2.appspot.com',
    messagingSenderId: '984192972928',
    appId: '1:984192972928:web:5d84dc94fe4e07da810e65',
    measurementId: 'G-V6CSC720EE'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
